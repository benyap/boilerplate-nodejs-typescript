# Boilerplate - Nodejs with Typescript

This is a project which contains all the boilerplate code necessary to kick-start a project in Nodejs with TypeScript. Comes preinstalled with the following: 

- `TypeScript` build and watch scripts

- `TSLint` to ensure a consistent style across the code

- `Jest` for testing

- `commitlint` to ensure good commit messages

- `.gitlab-ci.yml` sample configuration to help set up CI/CD on Gitlab


## Project

### Setting up your environment

```bash
# Clone the repository to your local machine
git clone git@gitlab.com:bwyap/boilerplate-nodejs-typescript.git

# Step into local repo
cd boilerplate-nodejs-typescript

# Install dependencies 
npm install
```

### Running the project

To run the repository locally, ensure you have the following environment variables in `.env` in the root directory, using actual values. 
**Do not commit this file.**

```ini
# Insert actual values in .env file

NODE_ENV=
```

Then use the following command to start the project

```bash
npm run watch
```

This command will start the project. Your code will be transpiled using `TypeScript` and watched for changes. 

### Building the project

This command will build the project and output it to the `dist` directory

```bash
npm run build
```

You can then run the build using the following command

```bash
npm start
```


## Contributing

There are a number of conventions that should be followed when developing this project. 
Please see the [CONTRIBUTING](CONTRIBUTING.md) guide for more details. 


## License

This project is licensed under the Apache License, Version 2.0.

See the [LICENSE](LICENSE) for more details. 
