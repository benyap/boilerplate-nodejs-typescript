# Contributing guide

Please follow the following conventions when contributing to this project on Gitlab. 

## Committing

Commit messages are linted using `commitlint` and is automatically checked when you use `git commit -m "<YOUR MESSAGE>"`. 
They must have the following format:

```text
type: subject
body?
footer?
```

Where `type` is one of the following: 

```
feat      (new feature for the user, not a new feature for build script)
fix       (bug fix for the user, not a fix to a build script)
docs      (changes to the documentation)
style     (formatting, missing semi colons, etc; no production code change)
refactor  (refactoring production code, eg. renaming a variable)
test      (adding missing tests, refactoring tests; no production code change)
chore     (updating build tasks etc; no production code change)
ci        (changes to CI configuration and scripts)
revert    (reverts a previous commit)
```

The `subject` is a short phrase that describes the changes made.
The `body` of the commit message contains a more detailed description of the changes made in the commit, and **why they were made**. 
The `footer` may contain any important notes or links relating to the changes made in the commit.

The `body` and `footer` of the commit message are optional. 

An example commit message:

```text
fix: remove unused variables
```

You can use the following prompt to help guide you through your submitting your commit message.

```bash
npm run commit
```

Type `:skip` if you wish to skip entering the body and footer sections when using the prompt.


## Style & Linting

This codebase adheres to the standard [TSLint recommended style guide](https://github.com/palantir/tslint/blob/master/src/configs/recommended.ts).
Additional configuration is found in the `tsling.json` file in the root directory.

Commits and pushes are blocked if your code does not pass linting. 
Check if your source code is compliant by running: 

```bash
npm run lint
```

NOTE: If configured correctly, your code editor should also be able to highlight linting errors inline.


## Running tests

Use the following command to run tests. 
All tests must pass before pushing to the repository. 

```bash
# Run all tests
npm run test

# Run unit tests only
npm run test:unit

# Run integration tests only
npm run test:int
```

Unit test files should be named using the format `*.unit.spec.js`.
Integration test file shouldbe named using the format `*.int.spec.js`.


## Pushing 

The following pre-push checks are automatically run when you use `git push`: 

1. Code linting must pass (see the Style & Linting section)

If these conditions fail, your push will be rejected. 
Pushes will also trigger a pipeline that will run tests on the CI server. 
Merge requests will be rejected if the pipeline fails. 
**It is recommended that you run the tests locally before pushing.**

## Branching 

All new branches should be created off the latest commit off the `develop` branch. 
Feature branch names should follow the format `<TYPE>/<DESCRIPTION>`, 
where `TYPE` is one of: 

- `feature` - for adding new features

- `fix` - for bug fixes

- `build` - for managing build configuration and deployment

and `DESCRIPTION` is a short phrase which describes the changes that will be made on the branch. 


## Merge requests

The following branches are protected and can only be merged using a merge request: 

- `develop`

- `staging`

- `master`

Only the `develop` branch should only be merged into `staging`,
and only the `staging` branch should be merged into `master`. 

**Merge requests must pass linting and tests before merging.** 
This is enforced using Gitlab pipelines.
If the pipeline fails, the merge request is rejected. 
Branches are configured to automatically deploy to their respective environments once a merge is successful using Gitlab CI/CD,
so it is important that these branches are kept in a stable, deployable state. 
