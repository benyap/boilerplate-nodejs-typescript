import * as dotenv from 'dotenv';

// Logging helper function
function logMessage(message: string) {
  console.log(message);
}

// Load environment variables from .env file

dotenv.config();

logMessage('Hello world!');
logMessage(`Current environemnt: ${process.env.NODE_ENV}`);


// A simple counter that uses async/await

async function sleep(ms: number) {
  await new Promise((resolve: () => void, reject: () => void) => {
    setTimeout(resolve, ms);
  });
}

async function counter() {
  let count = 0;
  while (true) {
    logMessage(`Count: ${count++}`);
    await sleep(1000);
  }
}

counter();
